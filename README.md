[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687235.svg)](https://doi.org/10.5281/zenodo.4687235)

# generic typical day hourly profiles dependent on temperature and country for domestic hot water demand in the tertiary sector




## Repository structure
Files:
```
data/hotmaps_task_2.7_load_profile_tertiary_shw_generic.csv          -- contains the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## Documentation


This dataset provides the hourly heat demand for domestic hot water in the tertiary sector for the EU-27, UK+CH+LI. 
As CH + LI are not part of the EU, they are not specifically modeled. We, thus, used the generic profiles of AT for both countries.
The profiles can be used to assemble a yearlong demand profile for a NUTS2 region and a specific year.
The profiles are type-day-dependent. We assume no temperature/season-dependency.
The columns “day type” refers to the type of a day in the week:
- weekdays = typeday 0;
- saturday or day before a holiday = typeday 1;
- sunday or holiday = typeday 2
These profiles combine profiles for subsectors of the tertiary sector based on the hourly energy demand per subsector for the NUTS-0 region in which a NUTS2 region is located (see report below)

For detailed explanations, a graphical illustration of the dataset and information on missing data for islands/autonomous regions please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.7 page 121ff.


## Limitations of the datasets

The datasets provided have to be interpreted as simplified indicator to enable the analysis that can be carried out within the Hotmaps toolbox. It should be noted that the results of the toolbox can be improved with recorded heat demand data of local representatives of the tertiary sector.

## References
[1] [Harmonised European time use surveys](http://ec.europa.eu/eurostat/web/products-manuals-and-guidelines/-/KS-RA-08-014) Eurostat, 2009, checked on 2/1/2018.

## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors

Matthias Kuehnbach, Simon Marwitz, Anna-Lena Klingler <sup>*</sup>,

<sup>*</sup> [Fraunhofer ISI](https://isi.fraunhofer.de/)
Fraunhofer ISI, Breslauer Str. 48, 
76139 Karlsruhe


## License


Copyright © 2016-2018: Matthias Kuehnbach, Anna-Lena Klingler, Simon Marwitz

Creative Commons Attribution 4.0 International License

This work is licensed under a Creative Commons CC BY 4.0 International License.


SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
